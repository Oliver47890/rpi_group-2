# RPI_Group 2 



## Members

@Reuben_Badham <br>
@Oliver47890

## About the project

The RPi Queue project is designed to replace the current paper roll ticket system that many pharmacies, bakeries and package shops implement all over Denmark and many other countries. 
The team will have to create a system that runs on a raspberry pi, that shows a QR code. Once the QR code is scanned the user will be redirected to a node red dashboard where they can see their place number in the queue and be told an estimated time until their number is called. 
An optional additional deliverable is to have the QR code refresh every couple of minutes to stop people abusing the system.  
