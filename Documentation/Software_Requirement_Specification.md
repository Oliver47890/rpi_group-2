## Outline
### Introduction
- Purpose : The purpose of the project is to reduce the amount of paper waste produced by the normal queue systems that can be found in bakeries, pharmacies and package shops
- Intended Audience : The audience are people who use queue systems in bakeries, pharmacies and package shops. 
- Intended Use : The system is intended to reduce the paper waste that using paper rolls of numbered tickets creates. In turn this should save companies money and time on replenishing the rolls. Also a decrease in device maintenance.
- Scope : The system will run a program that shows a QR code that sends the user to a NodeRED dashboard. From the dashboard the user should be able to see their place in the queue and see an estimated wait time. 

### Overall Description
- User Needs : The user needs are that the GUI (Graphical User Interface) is easy to understand and navigate. The user would also require that the system isn’t broken or faulty when they come to use it. The user should be able to see their queue time on a NodeRED dashboard that is easy for the user to understand and read clearly. 
- Assumptions and Dependencies : The assumptions and dependencies of the project are as follows. The system is based on technology that is widely available and cost effective. The system will be running a program, coded in Python on a flavour of Linux.
- The system is dependent on a functioning connection to the internet, as such the queueing system would malfunction were this connection to be cut or otherwise be interrupted, that is to say it is imperative that the store has a reliable and secure internet connection.
System Features and Requirements
- Functional Requirements : The system should shows the QR code on a screen. Once scanned the QR code should lead the user to the NodeRED dashboard. There the user can see their place in the queue. 
- External Interface Requirements : The system will have no external interface requirements. 
- System Features : The system will feature a QR code and should be usable by people of any age or ability. 
- Nonfunctional Requirements : The system should have a high level of security. No malicious user should be able to access the system and change code or tamper with the program. This is of high importance as without proper care, anyone could tamper with the QR code and redirect users to another malicious website, or act as a honey pot for data collection.<br> 
The system should be scalable so that if any adjustments were required, then it would be possible.<br>
The system should be reliable and not require maintenance.<br> 
The system should be usable by everyone, regardless of age or disability.<br>  

## Products Purpose    
### Intended Audience and Intended Use
This product is for users that are able to use their phone, instead of requiring a paper ticket in queue. 
### Product Scope
<strong>This product should have :<strong>
1. Shown QR code to customer
2. Assigned Number to a customer (paperless)

<strong>Should not have :<strong>
1. Vulnerable linking. (leading to websites with malware, viruses)

### Definitions and Acronyms 
- GUI : Graphical user interface
- RPi : Raspberry Pi 
- QR code: Quick Response Code
- LCD screen : Liquid Crystal Display 


## Project Description

### User Needs
The product is intended to be used in settings where a paperless queueing solution is desired. The product is simple in design and easy to implement in stores of many types, be that pharmacies, grocery stores etc. The system has an intuitive interface that allows anyone to understand the GUI.

### Assumptions and Dependencies
The system is based on current technology that is widely available, both to consumers and industry professionals. The system is utilising a raspberry pi as its main processing unit, as such the system will be based on a linux operating system. 
The system is dependent on a functioning connection to the internet, as such the queueing system would malfunction were this connection to be cut or otherwise be interrupted, that is to say it is imperative that the customer has a reliable and secure internet connection.

## Specific requirements

### Functional Requirements

The user will be able to scan a QR code and then get a number in the queue for example in stores or in Pharmacy etc..<br>
Every age should be able to use it, disabled users as well for example blind people.
A smartphone is required to use the QR code but you are also able to get the paper version to get in the queue if you don't have a smartphone .

### External Interface Requirements

- User : Each part of the user interface intends to be as user friendly as possible. 
- Hardware : Raspberry Pi 4
- Software : 
- Operating System : Rasbian
- Development Tool : Node RED, VS Code,
- Communications : The customer should be able to contact the system administrator in cases of emergencies.
### System Features 


### Other Nonfunctional Requirements

- Performance : The system should be able to perform as intended throughout the day without breaking down or requiring maintenance.
- Reliability : The system should be reliable and not require maintenance. 
- Security : The system should have a high level of security. No malicious user should be able to access the system and change code or tamper with the program. This is of high importance as without proper care, anyone could tamper with the QR code and redirect users to another malicious website, or act as a honey pot for data collection. 
- Availability : The system should be usable by everyone, regardless of age or disability. 


## Approval

*Awaiting approval*






